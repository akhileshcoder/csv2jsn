// Check if webapp is allowed or not in iframe
var url = require("url");
var got = require("got");
var fs = require("fs");
var path = require("path");
var metascraper = require("metascraper");

var myJson = require("./topWeb.json");

var maxCheck = myJson.length;
//var maxCheck = 10;

function isAllowedInIframe(header, origin) {
    Object.keys(header).forEach(i => header[i.toLowerCase()] = header[i]);
    if (header["x-frame-options"]) {
        let options = header["x-frame-options"].toLowerCase();
        if (options === "sameorigin") {
            return false;
        } else if (options === "deny") {
            return false;
        } else if (options.indexOf("allow-from") > -1) {
            let allowedOrigins = options.split(" ").slice(1);
            return allowedOrigins.find(_o => url.parse(_o).host === url.parse(origin).host);
        }
    }
    if (header["content-security-policy"]) {
        let _options = header["content-security-policy"].toLowerCase();
        let CheckOriginInSource = (source_name) => {
            _options = _options.split(";");
            _options = _options.find(_o => _o.indexOf(source_name) > -1);
            _options = _options.split(" ").slice(1);
            return _options.find(_o => url.parse(_o).host === url.parse(origin).host);
        }
        if (_options.indexOf("frame-src") > -1) {
            return CheckOriginInSource("frame-src");
        } else if (_options.indexOf("child-src")) {
            return CheckOriginInSource("child-src");
        } else if (_options.indexOf("default-src")) {
            return CheckOriginInSource("default-src");
        } else {
            return true;
        }
    } else {
        return true;
    }
}


var parsedData = [];
var getDta = async (targetUrl) => {
    try {
        const {body: html, headers, url} = await got(targetUrl.Domain);
        const metadata = await metascraper({html, url});
        targetUrl.metadata = metadata;
        targetUrl.iframe = metadata;
        targetUrl.iframe = isAllowedInIframe(headers, url);
    } catch (err) {
        console.log(err);
    }
    console.log("---");
    console.log(new Date());
    console.log("--");
    console.log(targetUrl);
    parsedData.push(targetUrl);
};
var slcSiz = 5;
var counter=0;
function runProm(fromInd){
    var end = fromInd+slcSiz;
    var romArr = (myJson.slice(fromInd,end)).map(i3 =>getDta(i3));
    Promise.all(romArr).then(resolve=>{
        let data = JSON.stringify(parsedData,null,4);
        fs.writeFile(path.resolve(__dirname,'./parsed/topParsedWeb'+counter+'.json'), data, function (err) {
            if (err) throw err;
            if(end<maxCheck){
                if(parsedData.length>40){
                    counter++;
                    parsedData =[];
                }
                runProm(end);
            }
            else console.log("All file written");
        });
    })
}
runProm(0);

