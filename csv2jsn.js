var path = require('path');

var fs = require('fs');
const csvFilePath= path.resolve(__dirname,'./topWeb.csv');
var JSONStream = require("JSONStream").stringifyObject("{",",","}")
/*
const csv=require('csvtojson')
csv().fromFile(csvFilePath)
    .then((jsonObj)=>{
        console.log(jsonObj);
        fs.writeFile(path.resolve(__dirname,'./topWeb.json'),JSON.stringify(jsonObj,null,4))
    })*/

var LineByLineReader = require('line-by-line'),
    lr = new LineByLineReader(csvFilePath);

lr.on('error', function (err) {
    // 'err' contains error object
    console.log("err: ",err);
});

var jsonObj = [];
var count = 0;

lr.on('line', function (line) {
    line = line.split(',');
    console.log(count++);
    jsonObj.push({
        GlobalRank:line[0],TldRank:line[1],Domain:line[2]
    })

});

lr.on('end', function () {
    jsonObj.shift();
    fs.writeFile(path.resolve(__dirname,'./topWeb.json'),JSON.stringify(jsonObj,null,4),function () {
        console.log('All lines are read, file is closed now.')
    })
});